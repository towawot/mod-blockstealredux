Scriptname PreventsAccident_QST extends ReferenceAlias  

bool  Property IsMultiTapSetting auto
bool  Property IsSneakSetting auto
int   Property maxClickCount = 2 auto
float Property delayTime = 0.3 auto

import PreventsAccidentPickUp

event Oninit()
	OnPlayerLoadGame()
endEvent

event OnPlayerLoadGame()
	maxClickCount = GetMultiTapCount()
	if (maxClickCount == 0)
		maxClickCount = 2
	endif
	delayTime = GetMultiTapTimer()
	if (delayTime == 0.0)
		delayTime = 0.3
	endif

; 	utility.wait(2)
; 	Debug.Notification("maxClickCount = " + maxClickCount)
; 	Debug.Notification("delayTime = " + delayTime)

endEvent
