Scriptname PreventsAccident_CONT extends ObjectReference  

PreventsAccident_QST Property QST auto

event OnActivate(ObjectReference akActionRef)
; 	UnregisterForMenu("ContainerMenu activate")
	RegisterForMenu("ContainerMenu")
endEvent

event OnMenuClose(String MenuName)
	if (MenuName == "ContainerMenu")
		UnregisterForMenu("ContainerMenu")

		Formlist blockList = Game.GetFormFromFile(0x00184C, "PreventsAccidentPickUp.esp") as FormList
		ObjectReference blockListContainer= Game.GetFormFromFile(0x0012E6, "PreventsAccidentPickUp.esp") as ObjectReference
		blockList.revert()
		blockListContainer.GetAllForms(blockList)
; 		int size = blockList.GetSize()
; 		Debug.Notification("register : " + size)
	endif
endEvent
