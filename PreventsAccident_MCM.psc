Scriptname PreventsAccident_MCM extends SKI_ConfigBase  

PreventsAccident_QST Property QST auto
import PreventsAccidentPickUp

int[]  _ID
bool[] _SET

int function GetVersion()
	return 2
endFunction

Event OnVersionUpdate(int a_version)
	if (a_version >= 2 && CurrentVersion < 2)
		ModName = "$Blocksteal Redux"
		_ID  = new int[10]
		_SET = new bool[10]
		Actor playerRef = Game.GetPlayer()
		Perk thisPerk = Game.GetFormFromFile(0x00D63, "PreventsAccidentPickUp.esp") as Perk
		if (thisPerk)
			_SET[0] = playerRef.HasPerk(thisPerk)
		endif
		_SET[1] = QST.IsSneakSetting
		_SET[2] = QST.IsMultiTapSetting
		_SET[3] = false
	endif
endEvent

Event OnConfigInit()
	ModName = "$Blocksteal Redux"
	_ID  = new int[10]
	_SET = new bool[10]

	Actor playerRef = Game.GetPlayer()
	Perk thisPerk = Game.GetFormFromFile(0x00D63, "PreventsAccidentPickUp.esp") as Perk
	if (thisPerk)
		_SET[0] = playerRef.HasPerk(thisPerk)
	endif
	_SET[1] = QST.IsSneakSetting
	_SET[2] = QST.IsMultiTapSetting
endEvent

Event OnConfigOpen()
	_SET[3] = false
	_SET[4] = false
	_SET[5] = false
endEvent
	
Event OnPageReset(String a_Page)
	SetCursorFillMode(TOP_TO_BOTTOM)
	_ID[0] = AddToggleOption("$_Activate", _SET[0])
	_ID[1] = AddToggleOption("$Sneaking", _SET[1])
	_ID[2] = AddToggleOption("$MultiTap", _SET[2])
	SetCursorPosition(1)
	_ID[3] = AddToggleOption("$Register Basket", _SET[3])
	_ID[4] = AddToggleOption("$Save Basket", _SET[4])
	_ID[5] = AddToggleOption("$Load Basket", _SET[5])
endEvent

bool function toggleSet(bool bbb)
	return !bbb
endFunction

; function SaveBasket()
; endFunction
; function LoadBasket()
; endFunction

event OnOptionSelect(int a_option)
	int index = _ID.find(a_option)
	if (index > -1)
		ObjectReference blockListContainer= Game.GetFormFromFile(0x0012E6, "PreventsAccidentPickUp.esp") as ObjectReference
		if (index == 3)
			bool continue = ShowMessage("$Do you open the Register basket?", true, "$_YES", "$_NO")
			if (continue)
				CloseJournalMenu()
				blockListContainer.Activate(Game.GetPlayer())
			endif
		elseif (index == 4)
			bool continue = ShowMessage("$Do you want to SAVE basket items?", true, "$_YES", "$_NO")
			if (continue)
				SaveBasket()
			endif
		elseif (index == 5)
			bool continue = ShowMessage("$Do you want to LOAD basket items?", true, "$_YES", "$_NO")
			if (continue)
				bool itemCount = (blockListContainer.GetNumItems() > 0)
				if (itemCount)
					continue = ShowMessage("$Will overwrite the current settings.", true, "$_OK", "$_CANCEL")
					if (continue)
						blockListContainer.RemoveAllItems()
						Formlist blockList = Game.GetFormFromFile(0x00184C, "PreventsAccidentPickUp.esp") as FormList
						blockList.revert()
						LoadBasket()
						blockListContainer.AddItem(blockList)
					endif
				else
					blockListContainer.RemoveAllItems()
					Formlist blockList = Game.GetFormFromFile(0x00184C, "PreventsAccidentPickUp.esp") as FormList
					blockList.revert()
					LoadBasket()
					blockListContainer.AddItem(blockList)
				endif
			endif
		else
			bool result = toggleSet(_SET[index])
			SetToggleOptionValue(a_option, result)
			_SET[index] = result
		endif
	endif
endEvent

event OnOptionHighlight(int a_option)
	int index = _ID.find(a_option)
	if (index > -1)
		if (index == 0)
			SetInfoText("$Activate this mod")
		elseif (index == 1)
			SetInfoText("$You can pick up in sneak mode.")
		elseif (index == 2)
			SetInfoText("$You can pick up item if activated after activate during a period of 0.3 seconds.")
		elseif (index == 3)
			SetInfoText("$The basket is activation blocker register without steal mark.")
		elseif (index == 4)
			SetInfoText("$Save basket items to the text.")
		elseif (index == 5)
			SetInfoText("$Load basket items from the text.")
		endif
	endif
endEvent

Event OnConfigClose()
	Actor playerRef = Game.GetPlayer()
	QST.IsSneakSetting = _SET[1]
	QST.IsMultiTapSetting = _SET[2]

	Perk thisPerk = Game.GetFormFromFile(0x00D63, "PreventsAccidentPickUp.esp") as Perk
	if (thisPerk)
		if (_SET[0])
			bool hasThisPerk = playerRef.HasPerk(thisPerk)
			if (!hasThisPerk)
				playerRef.AddPerk(thisPerk)
			endif
		else
			playerRef.RemovePerk(thisPerk)
		endif
	endif
EndEvent
