;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 4
Scriptname PreventsAccident_PRK Extends Perk Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akTargetRef, Actor akActor)
;BEGIN CODE
	;盗み防止用
	RunScript(akTargetRef, akActor)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_2
Function Fragment_2(ObjectReference akTargetRef, Actor akActor)
;BEGIN CODE
	;フォームリストにあるオブジェクト用
	RunScript(akTargetRef, akActor)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Function RunScript(ObjectReference akTargetRef, Actor akActor)
;BEGIN CODE
	if (akTargetRef)
		if (QST.IsMultiTapSetting)
			;マルチタップ時に別refだったらcountを初期化
			if (privateRef != akTargetRef)
				count = 0
			endif

			;マルチタップ設定のためアイテムを記録
			privateRef = akTargetRef
			count += 1
			
			;Sneakセクション処理中にOnUpdateによってcountが初期化された時対策
			;プライベート変数に退避させとく
			privateCount = count
			if (updateLockCount == 0)
				updateLockCount += 1
				RegisterForSingleUpdate(QST.delayTime)
			endif
		endif

		if (QST.IsSneakSetting)
			bool bSneak = akActor.isSneaking()
			if (bSneak)
				privateCount = QST.maxClickCount
			endif
		endif

		;activate section
		if (privateCount >= QST.maxClickCount)
			bool IsBookObject = akTargetRef.GetBaseObject() as book
			if (IsBookObject)
				akActor.addItem(akTargetRef, abSilent = true)
			else
				akTargetRef.Activate(akActor)
			endif
			privateCount = 0
		endif
	endif
endFunction

Event OnUpdate()
	count = 0
	updateLockCount = 0
endEvent

PreventsAccident_QST Property QST auto

ObjectReference privateRef
int privateCount
int count
int updateLockCount
